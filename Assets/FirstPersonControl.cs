﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonControl : MonoBehaviour
{
    private Rigidbody rig;
    public float moveSpeed;
    void Start()
    {
        rig = GetComponent<Rigidbody>();
    }

    
    void Update()
    {
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");
        rig.velocity = new Vector3(x, 0, z) * moveSpeed;
        //这句注释则有bug，解决bug的原因是这句话手动刷新了Listener（摄像机）的位置
        Camera.main.transform.position = transform.position;
    }
}



