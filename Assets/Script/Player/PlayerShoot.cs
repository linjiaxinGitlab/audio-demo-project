﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    public AK.Wwise.Event shotSound;
    public Weapon weapon;
    public Transform shotOrigin;
    private Vector3 target;
    [SerializeField]private float bulletSpeed = 100;

    [SerializeField] private Animator anim;
    [SerializeField] private bool Fire;
    [SerializeField] private bool Aim;

    //摄像机
    public GameObject normalCam;
    public GameObject aimCam;

    //枪口特效
    public ParticleSystem muzzleFlash;

    public enum ShotPattern
    {
        simple_shot,
        bursts_shot
    }
    [Header("射击模式")]
    public ShotPattern shotPatten;

    void Start()
    {
        InputManager.OnMouseDown += OnShootHander;
        InputManager.OnMouseUpLeft += MouseUpLeftHander;
        InputManager.OnMouseUpRight += OnMouseUpRightHander;
        InputManager.OnMouseDownRight += OnMouseRightDownHander;
        InputManager.OnMouseRight += OnMouseRightHander;
        anim = PlayerManager.Instance.playerAnim;
    }


    /*private void SelectShotMode()
    {
        switch(shotPatten)
        {
            case ShotPattern.simple_shot :
                break;

            case ShotPattern.bursts_shot:
                break;
        }
    }*/


    //左键点击
    private void OnShootHander()
    {
        anim.SetLayerWeight(1, 1);
        anim.SetBool("Fire", true);
        Fire = true;
        muzzleFlash.Play();
        #region 实际子弹
        /*if(PlayerManager.Instance.equippedWeapon!=null)
        {          
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out RaycastHit hit))
            {
                target = hit.point;
                GameObject bullet =  Instantiate
                    (weapon.bulletPre, 
                    shotOrigin.position, 
                    Quaternion.identity);
                bullet.GetComponent<Rigidbody>().AddForce((target - shotOrigin.position) * bulletSpeed);
            }
            AkSoundEngine.PostEvent("Play_shotSound", gameObject);
            //weapon.PlayShootSound();
        } */
        #endregion
    }

    //左键松开
    private void MouseUpLeftHander()
    {
        Fire = false;
        anim.SetBool("Fire", false);
        muzzleFlash.Stop();
        if (!Aim)
        {
            anim.SetLayerWeight(1, 0);
        }      
    }

    //右键点击
    private void OnMouseRightDownHander()
    {
        Aim = true;
        normalCam.SetActive(false);
        aimCam.SetActive(true);
        anim.SetLayerWeight(1, 1);
        anim.SetBool("Aim", true);       
    }
    //右键持续
    private void OnMouseRightHander()
    {
        Aim = true;
    }
    //右键松开
    private void OnMouseUpRightHander()
    {
        Aim = false;
        aimCam.SetActive(false);
        normalCam.SetActive(true);    
        anim.SetBool("Aim", false);
        anim.SetLayerWeight(1, 0);
    }
}
