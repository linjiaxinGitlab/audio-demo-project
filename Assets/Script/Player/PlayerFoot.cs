﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFoot : MonoBehaviour
{
    public CheckMaterial materialChecker;
    public AK.Wwise.Event FootstepSound;

    public void PlayFootstepSound()
    {
        materialChecker.CheckSoundMaterial(gameObject);
       // Debug.Log("播放脚步声的时间为" + Time.time);
        FootstepSound.Post(gameObject);
    }
}
