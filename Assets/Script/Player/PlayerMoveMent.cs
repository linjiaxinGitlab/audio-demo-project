﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveMent : MonoBehaviour
{
    //移动跳跃相关
    public float moveSpeed;
    public float jumpSpeed;
    public LayerMask groundMask;
    public InputManager inputManager;
    [SerializeField] private float groundCheckDistance;
    [SerializeField] private bool isGround;
    [SerializeField] private Transform footTarget;
    [SerializeField] private Vector3 moveDirction;
    [SerializeField] private Rigidbody rig;
    [SerializeField] private Animator anim;

    //视角控制相关
    public Transform CameraFollow;
    public float rotationPower_X;
    public float rotationPower_Y;


    
    private void Start()
    {
        //订阅列表
        InputManager.OnMoveHold += OnMoveHoldEventHander;
        InputManager.OnMoveUp += OnMoveUpEventHander;
        InputManager.OnJump += OnJumpEventHander;

        inputManager = GameObject.Find("Managers").GetComponent<InputManager>();

        rig = PlayerManager.Instance.playerRig;
        anim = PlayerManager.Instance.playerAnim;

        //视角控制锁定
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void OnJumpEventHander()
    {       
        if(isGround)
        {     
            rig.velocity += new Vector3(0, jumpSpeed, 0);
            anim.SetBool("Jump",true);
        }         
    }

    private void OnMoveHoldEventHander(Vector2 input)
    {
        moveDirction = transform.forward * input.x;
        moveDirction += transform.right * input.y;
    }
    private void OnMoveUpEventHander()
    {
        moveDirction = Vector2.zero;
    }

    private void FixedUpdate()
    {
        CheckIsGround();
        float inputF = inputManager.inputVector.x;
        float inputL = inputManager.inputVector.y;
        if(isGround)
        {
            anim.SetFloat("input_F", inputF);
            anim.SetFloat("input_L", inputL);
            PlayerManager.Instance.inAir = false;
            if (moveDirction.magnitude > 0)
            {
                PlayerManager.Instance.isMoveing = true;
                rig.velocity = new Vector3(moveDirction.x * moveSpeed, rig.velocity.y, moveDirction.z * moveSpeed);                      
            }
            else
            {
                PlayerManager.Instance.isMoveing = false;
            }
        }
        else
        {
             anim.SetBool("Jump", false);
             PlayerManager.Instance.inAir = true;
        }
    }

    private void Update()
    {
        float rotateX = inputManager.rotationX;
        float rotateY = inputManager.rotationY;
        CameraFollow.rotation *= Quaternion.AngleAxis(rotateX * rotationPower_X, Vector3.up);
        CameraFollow.rotation *= Quaternion.AngleAxis(-rotateY * rotationPower_Y, Vector3.right);
        var angles = CameraFollow.localEulerAngles;
        angles.z = 0;
        var angle = CameraFollow.localEulerAngles.x;

        //Clamp
        if (angle > 180 && angle < 340)
        {
            angles.x = 340;
        }
        else if (angle < 180 && angle > 40)
        {
            angles.x = 40;
        }
        //修正
        CameraFollow.localEulerAngles = angles;
        transform.rotation = Quaternion.Euler(0, CameraFollow.rotation.eulerAngles.y, 0);
        //Reset y，防止无限旋转
        CameraFollow.localEulerAngles = new Vector3(angles.x, 0, 0);
    }




    private void CheckIsGround()
    {
        isGround = Physics.CheckSphere(footTarget.position, groundCheckDistance, groundMask);
    }
}
