﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckMaterial : MonoBehaviour
{
    private Transform selfTrans;
    public LayerMask mask;
    private RaycastHit hit;
    private Vector3 direction = Vector3.down;
    private Vector3 checkOffset = Vector3.up * 0.1f;
    private void Awake()
    {
        selfTrans = transform;
        if(gameObject.name == "B-toe_L")
        {
            PlayerManager.foot_L = this;
        }
        if(gameObject.name == "B-toe_R")
        {
            PlayerManager.foot_R = this;
        }
    }

    public void CheckSoundMaterial(GameObject checker)
    {
        if (Physics.Raycast(selfTrans.position + checkOffset, direction, out hit, mask))
        {
            SoundMaterial sm = hit.collider.gameObject.GetComponent<SoundMaterial>();
            if (sm != null)
            {
                sm.material.SetValue(checker);
            }
        }
    }
    
}
