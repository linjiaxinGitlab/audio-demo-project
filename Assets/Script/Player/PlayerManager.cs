﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public delegate void NewWeaponEvent();
public class PlayerManager : Singleton<PlayerManager>
{
    [Header("Player Objects")]
    public GameObject player;
    public Rigidbody playerRig;
    public Animator playerAnim;
    public Transform playerTrans;

    public static CheckMaterial foot_L;
    public static CheckMaterial foot_R;

    [Header("Player information")]
    public bool inAir ;
    public bool isMoveing ;
    public bool isShooting;
    

    [Header("Weapon information")]
    public GameObject startWeapon;
    public Weapon equippedWeaponInfo;
    public GameObject equippedWeapon;
    public Transform weaponSlot_Gun;

    private void Awake()
    {
        if(player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
        }
        if(player != null)
        {
            SetupPlayerConnections();
        }
    }

    private void SetupPlayerConnections()
    {
        playerRig = player.GetComponent<Rigidbody>();
        playerTrans = player.GetComponent<Transform>();
        playerAnim = player.GetComponent<Animator>();
        //footTarget = GameObject.Find("isGrounded").GetComponent<Transform>();
    }

}
