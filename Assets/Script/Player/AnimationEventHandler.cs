﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEventHandler : MonoBehaviour
{
    private PlayerFoot foot_L;
    private PlayerFoot foot_R;
    private void Awake()
    {
        GameObject footL = GameObject.Find("B-toe_L");
        GameObject footR = GameObject.Find("B-toe_R");
        if(footL!=null)
        {
            foot_L = footL.GetComponent<PlayerFoot>();
        }
        if(footR!=null)
        {
            foot_R = footR.GetComponent<PlayerFoot>();
        }             
    }

    //动画事件中的int值对应enum的值0 - 左脚 ， 1 - 右脚
    public enum FootSide { left , right};
    public void TakeFootstep(FootSide side)
    {   
        //只有正在移动的时候才能运行该事件
        if (!PlayerManager.Instance.inAir && PlayerManager.Instance.isMoveing)
        {
            if (side == FootSide.left)
            {
                if (foot_L.FootstepSound.Validate())
                {
                    //从左脚播放脚步声
                    foot_L.PlayFootstepSound();
                    
                }
                    
            }
            else if (side == FootSide.right)
            {
                if (foot_L.FootstepSound.Validate())
                {
                    //从右脚播放脚步声
                    foot_R.PlayFootstepSound();
                }          
            }
        }
    }
}
