﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public delegate void PlayerMovementEvent(Vector2 dir);
    public delegate void InputEvent();
    //Input events
    public static event PlayerMovementEvent OnMoveHold;
    public static event InputEvent OnMouseDown;
    public static event InputEvent OnMouseDownRight;
    public static event InputEvent OnMouseRight;
    public static event InputEvent OnMouseUpLeft;
    public static event InputEvent OnMouseUpRight;
    public static event InputEvent OnMoveUp;
    public static event InputEvent OnJump;
    public Vector2 inputVector;

    //鼠标输入
    public float sensitivity_X = 5f;
    public float sensitivity_Y = 5f;
    public float rotationY = 0;
    public float rotationX = 0;


    void Update()
    {
        float INPUT_HORIZONTAL = Input.GetAxis("Horizontal");
        float INPUT_VERTICAL = Input.GetAxis("Vertical");
        if(INPUT_HORIZONTAL!=0 || INPUT_VERTICAL!=0)
        {          
            inputVector = new Vector2(INPUT_VERTICAL,INPUT_HORIZONTAL);
            OnMoveHold(inputVector);
        }
        if (INPUT_HORIZONTAL == 0 && INPUT_VERTICAL == 0)
        {        
            inputVector = Vector2.zero;
            OnMoveUp();
        }
        if(Input.GetKeyDown(KeyCode.Space))
        {
            OnJump();
        }
        if (Input.GetMouseButtonDown(0))
        {
            OnMouseDown();
        }
        if(Input.GetMouseButtonUp(0))
        {
            OnMouseUpLeft();
        }
        if(Input.GetMouseButtonDown(1))
        {
            OnMouseDownRight();
        }
        if(Input.GetMouseButton(1))
        {
            OnMouseRight();
        }
        if(Input.GetMouseButtonUp(1))
        {
            OnMouseUpRight();
        }
        rotationX = Input.GetAxis("Mouse X") * sensitivity_X;
        rotationY = Input.GetAxis("Mouse Y") * sensitivity_Y;
    }
}
