﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car : MonoBehaviour
{
    public AK.Wwise.RTPC speed_car_player;
    public float strenth;
    public float speed;
    private Rigidbody rig;
    public bool openDoppler;
    private void Start()
    {
        rig = GetComponent<Rigidbody>();
        rig.velocity = transform.forward * speed;
    }
    private void Update()
    {
        if (openDoppler)
        {
            GameObject player = GameObject.FindGameObjectWithTag("Player");
            Vector3 v1 = player.transform.position - transform.position;
            Vector3 v2 = rig.velocity;
            float angle = Vector3.Angle(v1, v2);
            float Cos = Mathf.Cos(angle / Mathf.Rad2Deg);
            float value = Mathf.Abs((v2.magnitude * Cos));
            Debug.Log(value * strenth);
            speed_car_player.SetGlobalValue(value * strenth);
        }
        else {
            speed_car_player.SetGlobalValue(12.5f);
        }
        
    }
   /* private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && openDoppler)
        {           
           // speed_car_player.SetGlobalValue( * strenth );                  
        }
        else
        {
            speed_car_player.SetGlobalValue(12.5f);
        }
    }*/

}
