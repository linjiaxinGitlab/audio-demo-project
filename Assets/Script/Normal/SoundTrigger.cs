﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundTrigger : MonoBehaviour
{
    [Header("Wwise")]
    public AK.Wwise.Event triggerSound;
    [Header("Trigger Area")]
    [SerializeField] private Collider triggerArea;
    void Start()
    {
        triggerArea = GetComponent<Collider>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            triggerSound.Post(gameObject);
        }         
    }
}
