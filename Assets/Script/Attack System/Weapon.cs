﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    [Header("Wwise")]
    public AK.Wwise.Event shootSound;
    public GameObject bulletPre;

    [Header("射击模式")]
    public bool isAutoFire;
    
    [Header("武器数据")]
    public WeaponData weapon_data;

    private void Start()
    {
        InitShootState();
    }

    //初始化武器射击类型
    private void InitShootState()
    {
        var fireType = weapon_data.DefaultFireType;
        if(fireType == WeaponData.FireType.Auto){
            isAutoFire = true;
            return;
        }else if(fireType == WeaponData.FireType.Single){
            isAutoFire = false;
            return;
        }
    }

    public void Equip()
    {
        PlayerManager.Instance.equippedWeapon = this.gameObject;
        PlayerManager.Instance.equippedWeaponInfo = this;
    }

    public void PlayShootSound()
    {
        shootSound.Post(gameObject);
    }


}
