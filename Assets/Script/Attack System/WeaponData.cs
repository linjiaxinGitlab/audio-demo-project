﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//枪械数据
[CreateAssetMenu]
public class WeaponData : ScriptableObject
{
    public enum FireType
    {
        Auto,
        Single
    }

    #region 射击模式

    [Tooltip("枪械默认射击模式")] public FireType DefaultFireType = FireType.Auto;

    #endregion

    #region 射击间隔

    [Tooltip("单发模式_射击间隔")] public float Single_FireDelayTime = 0.5f;

    [Tooltip("连发模式_射击间隔")] public float Auto_FireDelayTime = 0;

    #endregion


    #region 换弹时间

    [Tooltip("换弹时间")] public float ReloadTime = 0;

    #endregion



}
