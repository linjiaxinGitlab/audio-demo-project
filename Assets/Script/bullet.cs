﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet : MonoBehaviour
{
    [Header("Wwise")]
    public AK.Wwise.Event impactSound;
    private bool allowPlaySound = true;
    void Start()
    {
        Destroy(gameObject, 3f);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<SoundMaterial>() != null && allowPlaySound)
        {
            SoundMaterial sm = collision.gameObject.GetComponent<SoundMaterial>();
            sm.material.SetValue(gameObject);
            impactSound.Post(gameObject);
            allowPlaySound = false;
        }
    }
}
