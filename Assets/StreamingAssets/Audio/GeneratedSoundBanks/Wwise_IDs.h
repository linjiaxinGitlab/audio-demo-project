/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID PLAY_BELL = 1945722105U;
        static const AkUniqueID PLAY_CAMPFIRE = 4000411161U;
        static const AkUniqueID PLAY_CAR = 2690797144U;
        static const AkUniqueID PLAY_DJ = 2296411708U;
        static const AkUniqueID PLAY_FOOTSTEP = 1602358412U;
        static const AkUniqueID PLAY_ONSHOTHIT = 2284361840U;
        static const AkUniqueID PLAY_SHOTSOUND = 4136866367U;
        static const AkUniqueID PLAY_SUMMONDOWN = 1874297639U;
        static const AkUniqueID PLAY_TELEPORT = 3785065891U;
    } // namespace EVENTS

    namespace SWITCHES
    {
        namespace PLAYERMOVEMENT
        {
            static const AkUniqueID GROUP = 1350496757U;

            namespace SWITCH
            {
                static const AkUniqueID RUN = 712161704U;
                static const AkUniqueID WALK = 2108779966U;
            } // namespace SWITCH
        } // namespace PLAYERMOVEMENT

        namespace SURFACEMATERIAL
        {
            static const AkUniqueID GROUP = 171170469U;

            namespace SWITCH
            {
                static const AkUniqueID GRASS = 4248645337U;
                static const AkUniqueID STONE = 1216965916U;
                static const AkUniqueID WATER = 2654748154U;
                static const AkUniqueID WOOD = 2058049674U;
            } // namespace SWITCH
        } // namespace SURFACEMATERIAL

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID GUNSHOT_SPPED = 3722434852U;
        static const AkUniqueID PLAYBACK_RATE = 1524500807U;
        static const AkUniqueID RPM = 796049864U;
        static const AkUniqueID SPEED = 640949982U;
        static const AkUniqueID SS_AIR_FEAR = 1351367891U;
        static const AkUniqueID SS_AIR_FREEFALL = 3002758120U;
        static const AkUniqueID SS_AIR_FURY = 1029930033U;
        static const AkUniqueID SS_AIR_MONTH = 2648548617U;
        static const AkUniqueID SS_AIR_PRESENCE = 3847924954U;
        static const AkUniqueID SS_AIR_RPM = 822163944U;
        static const AkUniqueID SS_AIR_SIZE = 3074696722U;
        static const AkUniqueID SS_AIR_STORM = 3715662592U;
        static const AkUniqueID SS_AIR_TIMEOFDAY = 3203397129U;
        static const AkUniqueID SS_AIR_TURBULENCE = 4160247818U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MYSOUNDBANK = 3104675574U;
        static const AkUniqueID OTHERBANK = 3518329675U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MOTION_FACTORY_BUS = 985987111U;
        static const AkUniqueID OBJECTS = 1695690031U;
        static const AkUniqueID PLAYER = 1069431850U;
        static const AkUniqueID SHOT = 251412229U;
        static const AkUniqueID WORLD = 2609808943U;
    } // namespace BUSSES

    namespace AUX_BUSSES
    {
        static const AkUniqueID FACTORY = 1375449673U;
        static const AkUniqueID LARGEROOM = 187046019U;
        static const AkUniqueID REFLECT = 243379636U;
    } // namespace AUX_BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID DEFAULT_MOTION_DEVICE = 4230635974U;
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
